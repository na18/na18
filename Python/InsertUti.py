from fonctionBd import villeExistante
from fonctionBd import dernierID
import psycopg2


def insertUti (id, mail, nom, prenom, numtel,mdp,dtn,solde,adresse, nomrue, numrue, ville, cp, pays):

	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Si la ville entrée n'existe pas on l'ajoute à la base de données
	if not villeExistante(ville,str(cp)):
		Vsql = "INSERT INTO Ville VALUES('"+ville+"',"+str(cp)+",'"+pays+"')"
		cur.execute(Vsql)

	# On ajoute l'adresse si elle n'existe pas déjà
	if adresse == dernierID("Adresse","id_adresse"):
		Asql = "INSERT INTO Adresse VALUES("+str(adresse)+","+str(numrue)+",'"+nomrue+"','"+ville+"',"+str(cp)+")"
		cur.execute(Asql)

	# On ajoute l'utilisateur
	Usql = "INSERT INTO Utilisateur VALUES("+str(id)+",'"+mail+"','"+nom+"','"+prenom+"',0"+str(numtel)+",'"+mdp+"','"+dtn+"',"+str(solde)+","+str(adresse)+")"
	cur.execute(Usql)

	# On commit la transaction
	conn.commit()
	
	# Fermeture de la connexion
	conn.close()