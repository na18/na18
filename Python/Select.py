import psycopg2

'''------------------------------------------------------------------------------------------------------
	Fonction uti:
		Description : Cette fonction affiche la liste des utilisateurs présent dans la base de données
		Paramètre : /
		Retour : /
------------------------------------------------------------------------------------------------------'''
def uti():
	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Commande SQL

	sql = "SELECT * FROM Utilisateur"
	cur.execute(sql)

	# Affichage du tableau

	# Entete
	titre=["ID","Mail","Nom","Prenom","Numero","Naissance","Solde","Adresse"]
	for i in range(8):
		if i ==0:
				titre[i]=titre[i].ljust(1,' ')
		if i == 1:
			titre[i]=titre[i].ljust(30,' ')
		else:
			titre[i]=titre[i].ljust(10,' ')
	print(titre[0],"|",titre[1],"|",titre[2],"|",titre[3],"|",titre[4],"|",titre[5],"|",titre[6],"|",titre[7],"|")
	
	# Separateur
	tire =""
	print(tire.ljust(10,'-'),"+",tire.ljust(30,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+")
	
	# Données
	raw = cur.fetchone()
	while raw:
		x=[]
		for i in range(9):
			x.append(str(raw[i])) #On converti en chaine de caractère
			if i ==0:
				x[i]=x[i].ljust(1,' ')
			if i == 1:
				x[i]=x[i].ljust(30,' ')
			if i ==4:
				x[i]=x[i].ljust(9,' ')
			else:
				x[i]=x[i].ljust(10,' ') #Ajustement
		print(x[0],"|",x[1],"|",x[2],"|",x[3],"| 0%s"%x[4],"|",x[6],"|",x[7],"|",x[8],"|")
		raw = cur.fetchone()
	print()	
	# Fermeture de la  connexion
	conn.close()




'''------------------------------------------------------------------------------------------------------
	Fonction annonce:
		Description : Cette fonction affiche la liste des annonces visibles présentes dans la base de données
		Paramètre : /
		Retour : /
------------------------------------------------------------------------------------------------------'''

def annonce():
	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Commande SQL

	sql = "SELECT * FROM vAnnonceVisible"
	cur.execute(sql)

	# Affichage du tableau
	
	# Entete avec ajustement
	titre=["ID","Type","Date debut","Date fin","Intitule","Description","Stock","Produit","Ville","CP","Type Trans","Prix","PrixH","PrixB"]
	for i in range(14):
		if i ==4:
				titre[i]=titre[i].ljust(50,' ')
		if i ==5:
				titre[i]=titre[i].ljust(100,' ')
		else:
			titre[i]=titre[i].ljust(10,' ')
	print(titre[0],"|",titre[1],"|",titre[2],"|",titre[3],"|",titre[4],"|",titre[5],"|",titre[6],"|",titre[7],"|",titre[8],"|",titre[9],"|",titre[10],"|",titre[11],"|",titre[12],"|",titre[13],"|",)
	
	# Séparateur
	tire =""
	print(tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(50,'-'),"+",tire.ljust(100,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'),"+",tire.ljust(10,'-'))
	
	# Données avec ajustement
	raw = cur.fetchone()
	while raw:
		x=[]
		for i in range(16):
			x.append(str(raw[i]))
			if i==4 :
				x[i]=x[i].ljust(50,' ')
			if i==5 :
				x[i]=x[i].ljust(100,' ')
			else:
				x[i]=x[i].ljust(10,' ')

		print(x[0],"|",x[1],"|",x[2],"|",x[3],"|",x[4],"|",x[5],"|",x[6],"|",x[7],"|",x[8],"|",x[9],"|",x[12],"|",x[13],"|",x[14],"|",x[15])
		raw = cur.fetchone()
	print()	
	# Fermeture de la  connexion
	conn.close()


'''------------------------------------------------------------------------------------------------------
	Fonction transaction:
		Description : Cette fonction affiche la liste des transactions effectuées par un utilisateur
		Paramètre : -id de l'utilisateur
		Retour : /
------------------------------------------------------------------------------------------------------'''

def transaction(uti):
	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Commande SQL

	sql = "SELECT id_transaction,id_annonce,intitule_annonce,montant,date_transaction,nom_acheteur,nom_vendeur FROM vTransaction WHERE id_acheteur ="+str(uti)
	cur.execute(sql)

	# Affichage du tableau
	
	# Entete avec ajustement
	titre=["ID","Annonce","Intitule","Montant","Date","Nom acheteur","Nom vendeur"]
	for i in range(7):
		if i ==2:
				titre[i]=titre[i].ljust(50,' ')
		else:
			titre[i]=titre[i].ljust(15,' ')
	print(titre[0],"|",titre[1],"|",titre[2],"|",titre[3],"|",titre[4],"|",titre[5],"|",titre[6])
	
	# Séparateur
	tire =""
	print(tire.ljust(15,'-'),"+",tire.ljust(15,'-'),"+",tire.ljust(50,'-'),"+",tire.ljust(15,'-'),"+",tire.ljust(15,'-'),"+",tire.ljust(15,'-'),"+",tire.ljust(15,'-'))
	
	# Données avec ajustement
	raw = cur.fetchone()
	while raw:
		x=[]
		for i in range(7):
			x.append(str(raw[i]))
			if i==2 :
				x[i]=x[i].ljust(50,' ')
			else:
				x[i]=x[i].ljust(15,' ')

		print(x[0],"|",x[1],"|",x[2],"|",x[3],"|",x[4],"|",x[5],"|",x[6])
		raw = cur.fetchone()
	print()

	# Fermeture de la  connexion
	conn.close()



'''------------------------------------------------------------------------------------------------------
	Fonction conversation:
		Description : Cette fonction affiche la conversation entre 2 utilisateurs
		Paramètre : -id du premier utilisateur
					-id du second utilisateur
		Retour : /
------------------------------------------------------------------------------------------------------'''
def conversation(uti1,uti2):
	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Commande SQL

	sql = "SELECT M.date_mes, M.contenu_message, U.nom, U2.nom FROM (Message M JOIN Utilisateur U ON uti_en = U.id_uti) JOIN Utilisateur U2 ON uti_re = U2.id_uti WHERE uti_en ="+str(uti1)+ " AND uti_re ="+str(uti2)+" OR uti_en = "+str(uti2)+" AND uti_re ="+str(uti1)
	cur.execute(sql)

	# Affichage du tableau
	
	# Entete avec ajustement
	titre=["Date","Contenu","Nom emeteur","Nom recepteur"]
	for i in range(4):
		if i ==0:
			titre[i]=titre[i].ljust(21,' ')
		if i ==1:
				titre[i]=titre[i].ljust(100,' ')
		else:
			titre[i]=titre[i].ljust(15,' ')
	print(titre[0],"|",titre[1],"|",titre[2],"|",titre[3])
	
	# Séparateur
	tire =""
	print(tire.ljust(21,'-'),"+",tire.ljust(100,'-'),"+",tire.ljust(15,'-'),"+",tire.ljust(15,'-'))
	
	# Données avec ajustement
	raw = cur.fetchone()
	while raw:
		x=[]
		for i in range(4):
			x.append(str(raw[i]))
			if i ==0:
				x[i]=x[i].ljust(21,' ')
			if i==1 :
				x[i]=x[i].ljust(100,' ')
			else:
				x[i]=x[i].ljust(15,' ')

		print(x[0],"|",x[1],"|",x[2],"|",x[3])
		raw = cur.fetchone()
	print()

	# Fermeture de la  connexion
	conn.close()