import psycopg2

'''------------------------------------------------------------------------------------------------------
	Fonction dernierID:
		Description : Cette fonction renvois la valeur de l'identifiant suivant et inxistant de la table
		Paramètre : 
			-nom de la table
			-nom de l'attribut identifiant
		Retour : 
			-le nouvel identifiant
------------------------------------------------------------------------------------------------------'''
def dernierID (Table, id):

	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Récupération de la dernière id_adresse enregistré
	
	idSql="SELECT MAX("+id+") FROM "+Table
	cur.execute(idSql)
	idnb = cur.fetchone()[0]

	# Incrémentation de l'id

	idnb+=1

	# Fermeture de la connexion
	conn.close()

	return idnb

'''------------------------------------------------------------------------------------------------------
	Fonction adresseExistante:
		Description : Cette fonction renvois l'identifiant de l'adresse existante dans la bd ou un nouvel identifiant
		Paramètre : 
			-nom de la rue
			-numero de la rue
			-nom de la ville
			-code postal de la ville
		Retour : 
			-le nouvel identifiant ou l'identifiant de l'adresse existante
------------------------------------------------------------------------------------------------------'''
def adresseExistante (nom_rue,num_rue,ville,cp):
	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection adresse")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Test d'existence de l'adresse

	Asql= "SELECT id_adresse FROM Adresse WHERE nom_rue='"+nom_rue+"' AND num_rue="+num_rue+" AND ville_nom='"+ville+"' AND ville_cp="+cp
	cur.execute(Asql)

	try:
		idA=cur.fetchone()[0]
	except:
		idA=dernierID("Adresse","id_adresse")
	
	# Fermeture de la connexion
	conn.close()
	return idA


'''------------------------------------------------------------------------------------------------------
	Fonction villeExistante:
		Description : Cette fonction renvois 1 si la ville est dans la base de donnée 0 sinon
		Paramètre : 
			-nom de la ville
			-code postal de la ville
		Retour : 
			-0 si la ville n'existe pas
			-1 si elle existe
------------------------------------------------------------------------------------------------------'''
def villeExistante (nom,cp):
	# Initialisation des données de connexion

	HOST = "tuxa.sme.utc"
	USER = "bdd0a049"
	PASSWORD = "6SfasIpR"
	DATABASE = "dbbdd0a049"

	# Connexion à la base de données ou affichage d'une erreur et sortie du programme si la connexion échoue

	try:
		conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"%(HOST,DATABASE,USER,PASSWORD))
	except:
		print("Erreur de connection")
		exit()
	# Création d'un curseur pour les commandes sql

	cur=conn.cursor()

	# Test d'existence de la ville

	Vsql= "SELECT * FROM Ville WHERE nom='"+nom+"' AND cp="+cp
	cur.execute(Vsql)

	try:
		test = cur.fetchone()[0]
		return 1
	except:
		return 0

	conn.close()

