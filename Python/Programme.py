import psycopg2
import fonctionBd as fbd 
import InsertUti as I_uti
import Select as sl

# Fonctions

# Fonction qui affiche le menu des ajouts de données et retourne le choix
def menuInsert():
	choix=0
	print("*1- Inserer un nouvel utilisateur")
	print(" 2- Inserer une annonce")
	print(" 3- Poster un commentaire")
	print(" 4- Envoyer un message")
	print("\n","*Seule l'option avec une '*' est traitée dans ce programme\n")
	choix = int(input())
	print()
	return choix

# Fonction qui affiche le menu des selection de données et retourne le choix
def menuSelect():
	choix =0
	print("1- Afficher tout les utilisateurs")
	print("2- Afficher les annonces")
	print("3- Afficher les transactions pour un utilisateur")
	print("4- Afficher la conversation entre 2 utilisateurs")
	choix = int(input())
	print()
	return choix


#Programme principale

# On initialise le choix à 0
choix = 0

# On ne quitte le programme que quand l'utilisateur le souhaite
while choix!=3:

	# Affichage du menu
	print("---MENU---")
	print("1- Insertion de données")
	print("2- Selection de données")
	print("3- Sortir")
	choix =int(input())
	print()

	# Test des différents choix
	if choix == 1:
		insert=menuInsert()
		# On ne traite ici que le cas insert=1
		if insert == 1:
			
			# Incrémentation de l'id pour le nouvel utilisateur
			idUtilisateur= fbd.dernierID("Utilisateur","id_uti")

			# Récupération des informations
			print("Entrez les informations sur l'utilisateur")
			nomVille=input("Nom de la ville : ")
			cp=int(input("Code postal : "))
			pays=input("Pays : ")
			numRue=int(input("Numéro de la rue : "))
			nomRue=input("Nom de la rue : ")
			mail=input("Adresse mail : ")
			nom=input("Nom : ")
			prenom = input("Prenom : ")
			tel = int(input("Numero de telephone : 0"))
			mdp = input("Mot de passe : ")
			dtn = input("Date de naissance (AAAA-MM-JJ) : ")
			solde = float(input("Solde : "))

			# Récupération de l'identifiant d'adresse (nouveau si inexxistant)
			idAdresse= fbd.adresseExistante(nomRue,str(numRue),nomVille,str(cp))

			# Ajout des informations à la base de données et test de réussite d'ajout
			try:
				I_uti.insertUti(idUtilisateur,mail,nom,prenom,tel,mdp,dtn,solde,idAdresse,nomRue,numRue,nomVille,cp,pays)
				print("\nInsertion réussie avec succès\n")
			except:
				print("\nErreur d'insertion\n")
		
	# On affiche le menu de selection
	elif choix == 2:
		select = menuSelect()

		# Test des choix des affichages
		if select == 1:
			sl.uti() # Affichage des utilisateurs
		if select == 2:
			sl.annonce() # Affichage des annonces
		if select == 3:
			idUtilisateur = int(input("\nEntrez l'identifiant de l'utilisateur (ex : 1): "))
			print()
			sl.transaction(idUtilisateur) # Affichage des transactions
		if select == 4:
			idUti1 = int(input("\nEntrez l'identifiant de l'utilisateur 1 (ex : 6): "))
			idUti2 = int(input("\nEntrez l'identifiant de l'utilisateur 2 (ex : 7): "))
			print()
			sl.conversation(idUti1,idUti2) # Affichage de la conversation

	# On quitte le programme pour tout autre valeur de choix
	else:
		choix = 3