## Optimisation des SELECTS (et vues) suivants par orde de priorité d'optimisation 

    1-Annonces non supprimées (vue)
    2-Visualisation par rubriques
    3-Selection par mots clé
    4-Transaction (vue)
    5-Conversation (select)
    6-Vues des types d'annonces 
    7-Vue utilisateurs bloqués 
    
#### 1- Annonce non supprimées

On veut que cette vue soit bien optimisée pour un accès rapide et régulier à toutes les annonces en cours. Cependant on ne selectionne ici qu'une table, la seule optimisation possible serait une réduction du nombre d'attributs sélectionnés, en effet certains attributs ne sont pas utiles à l'utilisateur.
On ne peut donc pas énormément optimiser cette vue, il faudra cependant faire attention que les autres optimisations ne l'affectent pas.

#### 2- Visualisation par rubrique

On veut que cette sélection soit optimisée pour être rapide, en effet les recherches par rubrique sont assez fréquentes sur ce genre de site.
Cependant il est difficile d'optimiser plus cette sélection, on peut difficilement partitionner une table ni en dénormaliser une, on pourrait indexer les rubriques mais elles font déjà parti de la clé primaire donc sont déjà indexées.

#### 3- Selection par mots clé

Cette sélection est un peu identique à la sélection par rubrique, il est donc difficile de l'optimiser.

#### 4- Transaction

On veut que cette vue soit optimisée mais toujours sans impacter les SELECT vus précédement.
On peut premièrement partitionner horizontalement la classe proposition en fonction de l'état de la proposition, ce qui simplifierait la sélection des propositions finales retirant ainsi une vue intermediaire.
On peut ensuite indexer le nom et prénom des utilisateurs, car ceux ci sont beaucoup selectionnés dans cette vue pour connaitre le nom de l'acheteur et du vendeur.
On va donc créer deux tables une pour les propositions en cours et l'autre pour les propositions finales ainsi qu'un index pour le nom et le prénom de l'utilisateur.

#### 5- Conversation

On remarque ici qu'il n'y a pas grand chose à optimiser, on selectionne le nom et prénom des deux utilisateurs et on les affiche, l'indexation de ces deux attributs participe donc à l'optimisation de cette sélection.
Un partitionnement verticale de l'id_uti, du nom et du prénom de l'utilisateur n'aurait pas été pertinent car dans la vue Transaction on séléctionne toute la table utilisateur, cela ralentirait donc la vue.

#### 6- Vues des types d'annonces

Ces différentes vues sont faites à partir d'une sélection de la table annonce, une restriction est ensuite ajoutée afin de ne sélectionner que les annonces non supprimées. On peut donc faire les sélections à partir de la vue AnnonceVisible.
Ces vues n'ont pas besoin d'être beaucoup optimisées car dans l'utilisation de notre base de donnée elles ne seront pas beaucoup appellées.

#### 7- Vue utilisateurs bloqué

Cette vue ne nécessite pas d'être optimisée car elle n'est pas souvent recalculée, en effet il n'y a normalement pas des utilisateurs bloqués très régulièrement, l'indexation des nom et prénoms utilisateurs optimise cependant cette vue mais nous ne chercheront pas à l'otpimiser d'avantages.



### Remarque :

Nous avons décidé de baser le reste du projet les preuves de concepts majoritairement mais aussi les insertions de données, sur le SQL non optimisé. En effet les optimisations apportées sont relativement mineures et cela nous permet de présenter les changements réels effectués à notre base de donnée pour l'optimiser.