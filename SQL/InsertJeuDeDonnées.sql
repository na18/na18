/*Suppression de toutes les données avant ajout (facultatif si ajout d'un nouveau jeu de données non présent dans la base)*/
TRUNCATE TABLE Administrateur, Ville, Adresse, Utilisateur, Action, Message, Annonce, Commentaire, MotsCles, AnnonceMots, Rubrique, AcorrespondR, Proposition;

/*Ajout des données géré par des transaction, on cherche à faire un ajout de donnée cohérent dans le temps*/


    /*Ajout des Administrateurs de la base en premier lieu, chaque ajout correspond à une transaction*/
        
            INSERT INTO Administrateur VALUES (1); 
            INSERT INTO Administrateur VALUES (2); 
            INSERT INTO Administrateur VALUES (3);
            INSERT INTO Administrateur VALUES (4);
            INSERT INTO Administrateur VALUES (5);


/* On ajoutera ensuite des Utilisateurs, des annonces postés par ceux ci, des propositions, messages et commentaires. L'ordre importe peu ici, il faut cependant veiller à ce que l'on ajoute les utilisateurs concernés avant les actions qu'ils pourraient faire*/
/* On placera au sein d'une meme transaction les ajouts de villes, adresse et utilisateur ou encore de ville, annonce, MotsCles, Rubrique, AcorrespondR et AnnoncesMots pour des raisons de cohérences*/

    /*On débute par l'ajout de quelques utilisateurs (d'autres pourront etre ajoutés par la suite)*/
    
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Compiegne', 60200, 'FRANCE');
            INSERT INTO Adresse VALUES (1, 14, 'Rue des peupliers', 'Compiegne', 60200);
            INSERT INTO Utilisateur VALUES (1, 'aboudot@hotmail.com', 'Boudot', 'Adrien', 0785659215, 'mdpasse', '1989-08-08', 10, 1); 
        COMMIT;
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Cergy', 95800, 'FRANCE');
            INSERT INTO Adresse VALUES (2, 3, 'Avenue des cerisiers', 'Cergy', 95800);
            INSERT INTO Utilisateur VALUES (2, 'jcharles@hotmail.com', 'Charles', 'Joanne',0215846395, 'bleuciel', '1999/01/02', 120, 2); 
        COMMIT;
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Paris', 75100, 'FRANCE');
            INSERT INTO Adresse VALUES (3, 875, 'Boulevard Marvel', 'Paris', 75100);
            INSERT INTO Utilisateur VALUES (3, 'franck.allon@gmail.com', 'ALLON', 'Franck', 0612345765, 'bg@frnk','1975-07-14', 100, 3); 
        COMMIT;
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Carquefou', 44120, 'FRANCE');
            INSERT INTO Adresse VALUES (4, 22, 'Rue de la Loue', 'Carquefou', 44120);
            INSERT INTO Utilisateur VALUES (4, 'fanny.uziet@gmail.com', 'UZIET', 'Fanny', 0112345765, 'turtlespiz','2000-08-12', 23, 4);
        COMMIT;
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Nantes', 44110, 'FRANCE');
            INSERT INTO Adresse VALUES (5, 25, 'Rue de la Liberte', 'Nantes', 44110);
            INSERT INTO Utilisateur VALUES (5, 'marie.dupuis@gmail.com', 'DUPUIS', 'Marie', 0755126554, 'big0rneau', '1996-03-29', 30, 5); 
        COMMIT;
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Rennes', 35110, 'FRANCE');
            INSERT INTO Adresse VALUES (6, 450, 'Rue de la soif', 'Rennes', 35110);
            INSERT INTO Utilisateur VALUES (6, 'sdinard@hotmail.fr', 'Dinard', 'Simon', 0201456932, 'passedemot', '1985-10-25', 12, 6);
        COMMIT;

    /*On considère maintenant l'ajout de 2 annonces par 2 utilisateurs différents*/
        
        BEGIN TRANSACTION;
            INSERT INTO Annonce VALUES (1, 'Offre', '2019-05-15', NULL, 'Velo bleu', 'Vélo en bon état, 6 vitesses 2 plateaux, à venir chercher', 1, 'bien', 'Cergy', 95800, NULL, 2, 'affaire_normale', 50, NULL, NULL);
            INSERT INTO MotsCles VALUES ('Velo');
            INSERT INTO AnnonceMots VALUES (1, 'Velo');
            INSERT INTO MotsCles VALUES ('Bicyclette');
            INSERT INTO AnnonceMots VALUES (1, 'Bicyclette');
            INSERT INTO MotsCles VALUES ('Pédales');
            INSERT INTO AnnonceMots VALUES (1, 'Pédales');
            INSERT INTO Rubrique VALUES ('Sport');
            INSERT INTO AcorrespondR VALUES (1, 'Sport');
        COMMIT;
        BEGIN TRANSACTION;
            INSERT INTO Ville VALUES ('Lille',59000,'FRANCE');
            INSERT INTO Annonce VALUES (2, 'Offre', '2019-05-16', NULL, 'Chaussure', 'Basket nike 44 neuf', 1, 'bien', 'Lille', 59000, NULL, 3, 'affaire_normale', 150, NULL, NULL);
            INSERT INTO MotsCles VALUES ('Chaussure');
            INSERT INTO AnnonceMots VALUES (2, 'Chaussure');
            INSERT INTO MotsCles VALUES ('Nike');
            INSERT INTO AnnonceMots VALUES (2, 'Nike');
            INSERT INTO MotsCles VALUES ('Neuf');
            INSERT INTO AnnonceMots VALUES (2, 'Neuf');
            INSERT INTO Rubrique VALUES ('Vetements');
            INSERT INTO AcorrespondR VALUES (2, 'Vetements');
            INSERT INTO Rubrique VALUES ('Basket');
            INSERT INTO AcorrespondR VALUES (2, 'Basket');
            INSERT INTO AcorrespondR VALUES (2, 'Sport');
        COMMIT;
            
    /*On va maintenant insérer des échanges entre les utilisateurs à propos des annonces et ajouter des propositions*/
    
            INSERT INTO Commentaire VALUES (1, 'Je vous ai envoyé un mp', '2019-05-15', 1, 1);
            INSERT INTO Message VALUES (1,'2019-05-15''15:11','Bonjour, je suis intéressé par votre vélo, serait-il possible que vous le livriez ?',1,2);
            INSERT INTO Message VALUES (2,'2019-05-15''17:31','Bonjour, non comme cela est précisé dans l annonce il m est impossible de me déplacer',2,1);
            INSERT INTO Commentaire VALUES (1, 'Un peu cher pour des chaussures !', '2019-05-16', 4, 2);
            INSERT INTO Message VALUES (3,'2019-05-16''11:22','J ai trouvé de quoi me déplacer je viendrai le chercher',1,2);

        /*On considère une proposition finale comme un achat, l'annonce sera supprimé par un administrateur qui vérifiera que la transaction a bien été mené et qui la validera*/

            INSERT INTO Proposition VALUES (1, 'FINAL', 50, '2019-05-16', 1,NULL, 1);
            
        /*Mise à jour des soldes et suppression de l'annonce par l'administrateur 1*/
        BEGIN TRANSACTION;
            UPDATE Utilisateur SET solde = solde-50 WHERE id_uti = 1;
            UPDATE Utilisateur SET solde = solde+50 WHERE id_uti = 2;
            UPDATE Annonce SET admin_delete = '1' WHERE id_annonce = 1;
        COMMIT;
        
    /*On va maintenant simuler le bloquage d'un utilisateur et la suppression d'une annonce par un administrateur*/
    
        /*Ajout d'une Annonce non conforme par l'utilisateur 2*/
        BEGIN TRANSACTION;
            INSERT INTO Annonce VALUES (3, 'Offre', '2019-05-17', NULL, 'Arme à feu', 'Vente d armes 9mm conforment en bon etat', 5, 'bien', 'Cergy', 95800, NULL, 2, 'affaire_normale', 200, NULL, NULL);
            INSERT INTO MotsCles VALUES ('Arme');
            INSERT INTO AnnonceMots VALUES (3, 'Arme');
            INSERT INTO MotsCles VALUES ('9mm');
            INSERT INTO AnnonceMots VALUES (3, '9mm');
            INSERT INTO AcorrespondR VALUES (3, 'Sport');
        COMMIT;
        /*L'annonce n'est pas conforme et est gérée par un administrateur*/
        BEGIN TRANSACTION;
            INSERT INTO Action VALUES (1, '2019-05-17''12:42:17' ,'bloque', 'Ajout d une annonce non conforme', 3, 2);--On bloque l'utilisateur
            UPDATE Annonce SET admin_delete = '3' WHERE id_annonce =3; --Suppression de l'annonce
            DELETE FROM AnnonceMots WHERE annonce = 3; --Suppression des mots cles
            DELETE FROM MotsCles WHERE mot = 'Arme' OR mot= '9mm';
        COMMIT;
    
    /*On ajoute un utilisateur*/
        
        BEGIN TRANSACTION;
            INSERT INTO Adresse VALUES (7, 11, 'Rue Solferino', 'Lille', 59000);
            INSERT INTO Utilisateur VALUES (7, 'PMichar@laposte.net', 'Michar', 'Paul', 0654328756, 'GFDR45ez453GD*/dfs*', '1994-12-25', 800, 7); 
        COMMIT;
    
    /*On ajoute une suite de transaction pour avoir un jeu de donnée comportant tout les type de transaction dans les annonces et plus de données dans les autres tables*/
    
        /*Annonce en enchere*/
        BEGIN TRANSACTION;
            INSERT INTO Annonce VALUES (4, 'Offre', '2019-05-18', '2019-06-18', 'Tableau de Picasso', 'Tableau de Pablo Picasso de 1925 en bon ete, estimé par un expert', 1, 'bien', 'Lille', 59000, NULL, 7, 'enchere', NULL, 1000, NULL);
            INSERT INTO MotsCles VALUES ('Tableau');
            INSERT INTO AnnonceMots VALUES (4, 'Tableau');
            INSERT INTO MotsCles VALUES ('Peintre');
            INSERT INTO AnnonceMots VALUES (4, 'Peintre');
            INSERT INTO MotsCles VALUES ('Toile');
            INSERT INTO AnnonceMots VALUES (4, 'Toile');
            INSERT INTO Rubrique VALUES ('Art');
            INSERT INTO AcorrespondR VALUES (4, 'Art');
        COMMIT;
        

            INSERT INTO Commentaire VALUES (1,'Très beau tableau !','2019-05-19',6,4);

            INSERT INTO Message VALUES (4,'2019-05-19''15:23','Bonjour quelles sont les dimensions de ce tableau?',6,7);

            INSERT INTO Message VALUES (5,'2019-05-19''18:46','Bonjour le tableau est assez grand 123*89 cm',7,6);

            INSERT INTO Proposition VALUES (2,'En cours',1100,'2019-05-19',4,NULL,6);

        
        /*Annonce à négocier*/
        BEGIN TRANSACTION;
            INSERT INTO Annonce VALUES (5, 'Offre', '2019-05-20', NULL, 'Cours de Maths', 'Je propose des cours de maths niveau Lycée, 2h par semaine prix par heure négociabel', 1, 'service', 'Nantes', 44110, NULL, 5, 'a_negocier', NULL, NULL, 15);
            INSERT INTO MotsCles VALUES ('Cours');
            INSERT INTO AnnonceMots VALUES (5, 'Cours');
            INSERT INTO MotsCles VALUES ('Maths');
            INSERT INTO AnnonceMots VALUES (5, 'Maths');
            INSERT INTO Rubrique VALUES ('Education');
            INSERT INTO AcorrespondR VALUES (5, 'Education');
        COMMIT;
        
        /*Proposition pour l'annonce 4*/

            INSERT INTO Proposition VALUES (3,'En cours',1150,'2019-05-20',4,NULL,3);

            INSERT INTO Commentaire VALUES (1,'Très bon service, les cours de maths ont servi à mon fils !','2019-05-21',1,5);

            INSERT INTO Message VALUES (6,'2019-05-21''12:30','Bonjour est il possible pour vous d assurer des cours niveau 3eme?',4,5);

            INSERT INTO Message VALUES (7,'2019-05-21''14:16','Bonjour oui cela peut aussi ce faire sans soucis!',5,4);

        
        /*Proposition pour l'annonce 5*/

            INSERT INTO Proposition VALUES (4,'En cours',12,'2019-05-21',5,NULL,4);

            INSERT INTO Message VALUES (8,'2019-05-21''18:56','Votre prix me convient, combien souhaitez vous d heures de cours?',5,4);

            INSERT INTO Message VALUES (9,'2019-05-21''19:03','2h par semaines devrait être suffisant ! Réglons déjà la première semaine',4,5);
        
        /*L'utilisateur accepte le prix proposé*/
        BEGIN TRANSACTION;
            UPDATE Proposition SET etat = 'FINAL' WHERE id_proposition = 4;
            UPDATE Utilisateur SET solde = solde-24 WHERE id_uti = 4;
            UPDATE Utilisateur SET solde = solde+24 WHERE id_uti = 5;
           /*On ne supprime pas l'annonce ici car c'est un service sans stock limité*/
        COMMIT;
        
        /*Annonce échange*/
        BEGIN TRANSACTION;
            INSERT INTO Annonce VALUES (6, 'Demande', '2019-05-22', NULL, 'Echange instrument de musique', 'Je propose d echanger ma guitare acoustique par une guitare classique faites vos propositions !', 1, 'bien', 'Compiegne', 60200, NULL, 1, 'echange', NULL, NULL, NULL);
            INSERT INTO MotsCles VALUES ('Instrument');
            INSERT INTO AnnonceMots VALUES (6, 'Instrument');
            INSERT INTO MotsCles VALUES ('Musique');
            INSERT INTO AnnonceMots VALUES (6, 'Musique');
            INSERT INTO MotsCles VALUES ('Guitare');
            INSERT INTO AnnonceMots VALUES (6, 'Guitare');
            INSERT INTO AcorrespondR VALUES (6, 'Art');
        COMMIT;
        
        /*Proposition pour l'annonce 6*/

            INSERT INTO Proposition VALUES (5,'En cours',NULL,'2019-05-22',5,NULL,7);

        
        /*La proposition n'est pas accepté par l'utilisateur l'annonce reste en 'En Cours'*/
        
        /*Annonce Don*/
        BEGIN TRANSACTION;
            INSERT INTO Annonce VALUES (7, 'Offre', '2019-05-23', NULL, 'Donne lit', 'Je donne un lit 2 place en très bon etat pour cause de démenagement', 1, 'bien', 'Paris', 75100, NULL, 3, 'don', NULL, NULL, NULL);
            INSERT INTO MotsCles VALUES ('Lit');
            INSERT INTO AnnonceMots VALUES (7, 'Lit');
            INSERT INTO MotsCles VALUES ('Matelat');
            INSERT INTO AnnonceMots VALUES (7, 'Matelat');
            INSERT INTO Rubrique VALUES ('Mobilier');
            INSERT INTO AcorrespondR VALUES (7, 'Mobilier');
        COMMIT;
        
        
        /*On considère une réponse à l'annonce 2 par l'utilisateur 1*/

            INSERT INTO Proposition VALUES (6,'En cours',150,'2019-05-23',2,NULL,1);

        
        /*Le vendeur accepte la proposition*/
        BEGIN TRANSACTION;
            UPDATE Proposition SET etat = 'FINAL' WHERE id_proposition = 6;
            UPDATE Utilisateur SET solde = solde-150 WHERE id_uti = 1;
            UPDATE Utilisateur SET solde = solde+150 WHERE id_uti = 3;
            UPDATE Annonce SET admin_delete ='3' WHERE id_annonce = 2;
        COMMIT;
        
       