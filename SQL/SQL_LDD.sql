/*Suppression des tables*/
DROP TABLE IF EXISTS Proposition CASCADE;
DROP TABLE IF EXISTS AcorrespondR CASCADE;
DROP TABLE IF EXISTS Rubrique CASCADE;
DROP TABLE IF EXISTS Annonce CASCADE;
DROP TABLE IF EXISTS Commentaire CASCADE;
DROP TABLE IF EXISTS Message CASCADE;
DROP TABLE IF EXISTS Action CASCADE;
DROP TABLE IF EXISTS Utilisateur CASCADE;
DROP TABLE IF EXISTS Adresse CASCADE;
DROP TABLE IF EXISTS Ville CASCADE;
DROP TABLE IF EXISTS Administrateur CASCADE;
DROP TABLE IF EXISTS MotsCles CASCADE; 
DROP TABLE IF EXISTS AnnonceMots CASCADE; 

/*Suppression des Vues*/
DROP VIEW IF EXISTS vAdresseSansUti;
DROP VIEW IF EXISTS vANegocier;
DROP VIEW IF EXISTS vEchange;
DROP VIEW IF EXISTS vDon;
DROP VIEW IF EXISTS vAffaireNormale;
DROP VIEW IF EXISTS vEnchere;
DROP VIEW IF EXISTS vAnnonceVisible;


/*Suppression de la vue transaction*/
DROP VIEW IF EXISTS vTransaction CASCADE;
DROP VIEW IF EXISTS vTransaction5 CASCADE;
DROP VIEW IF EXISTS vTransaction4 CASCADE;
DROP VIEW IF EXISTS vTransaction3 CASCADE;
DROP VIEW IF EXISTS vTransaction2 CASCADE;
DROP VIEW IF EXISTS vTransaction1 CASCADE;
/*Suppression de la vue utilisateur bloqué*/
DROP VIEW IF EXISTS vUtilisateurBloque CASCADE;
DROP VIEW IF EXISTS vUtilisateurBloque2 CASCADE;
DROP VIEW IF EXISTS vUtilisateurBloque1 CASCADE;



CREATE TABLE Administrateur
(
	id_admin INTEGER PRIMARY KEY
);


CREATE TABLE Ville
(
	nom VARCHAR,
	cp INTEGER,
	pays VARCHAR NOT NULL,
	PRIMARY KEY (nom,cp)
);

CREATE TABLE Adresse
(
	id_adresse INTEGER PRIMARY KEY,
	num_rue INTEGER,
	nom_rue VARCHAR NOT NULL,
	ville_nom VARCHAR NOT NULL,
	ville_cp INTEGER NOT NULL,
	FOREIGN KEY (ville_nom,ville_cp) REFERENCES Ville(nom,cp)
);

CREATE TABLE Utilisateur
(
	id_uti INTEGER PRIMARY KEY,
	email VARCHAR NOT NULL,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	num_tel INTEGER,
	mot_de_passe VARCHAR NOT NULL,
	date_naissance DATE,
	solde REAL,
	adresse INTEGER NOT NULL,
	FOREIGN KEY (adresse) REFERENCES Adresse(id_adresse),
	UNIQUE (email)
);

/*Verification de la contrainte PROJECTION(Utilisateur,adresse)=PROJECTION(Adresse,id_adresse) avec une vue*/
CREATE VIEW vAdresseSansUti (id_adresse_sans_utilisateur) AS
    SELECT A.id_adresse
    FROM Adresse A LEFT JOIN Utilisateur U ON U.adresse = A.id_adresse
    WHERE id_uti IS NULL;
    
CREATE TABLE Action
(
	id_act INTEGER PRIMARY KEY,
	date_act TIMESTAMP NOT NULL, 
	type VARCHAR CHECK (type = 'bloque' OR type ='debloque'),
	raison VARCHAR,
	admin INTEGER NOT NULL,
	uti INTEGER NOT NULL,
	FOREIGN KEY (admin) REFERENCES Administrateur(id_admin),
	FOREIGN KEY (uti) REFERENCES Utilisateur(id_uti)
);

CREATE TABLE Message
(
	id_mes INTEGER PRIMARY KEY,
	date_mes TIMESTAMP NOT NULL, 
	contenu_message VARCHAR,
	uti_en INTEGER NOT NULL,
	uti_re INTEGER NOT NULL,
	FOREIGN KEY (uti_en) REFERENCES Utilisateur(id_uti),
	FOREIGN KEY (uti_re) REFERENCES Utilisateur(id_uti)
);


CREATE TABLE Annonce
(
	id_annonce INTEGER PRIMARY KEY,
	type_annonce VARCHAR CHECK(type_annonce = 'Offre' OR type_annonce =  'Demande'),
	date_debut DATE NOT NULL,
	date_fin DATE,
	intitule VARCHAR,
	description VARCHAR,
	stock INTEGER NOT NULL,
	type_produit VARCHAR CHECK (type_produit = 'bien' OR type_produit = 'service'),
	ville_nom VARCHAR NOT NULL,
	ville_cp INTEGER NOT NULL,
	admin_delete INTEGER,
	uti INTEGER NOT NULL,
	type_trans VARCHAR,
	prix REAL,
	prix_bas REAL,
	prix_haut REAL,
	FOREIGN KEY (ville_nom,ville_cp) REFERENCES Ville(nom,cp),
	FOREIGN KEY (admin_delete) REFERENCES Administrateur(id_admin),
	FOREIGN KEY (uti) REFERENCES Utilisateur(id_uti),
	CHECK ((prix IS NOT NULL AND prix_bas IS NULL AND prix_haut IS NULL AND type_trans = 'affaire_normale') OR (prix IS NULL AND prix_bas IS NOT NULL AND prix_haut IS NULL AND type_trans = 'enchere') OR (prix IS NULL AND prix_bas IS NULL AND prix_haut IS NOT NULL AND type_trans ='a_negocier') OR (prix IS NULL AND prix_bas IS NULL AND prix_haut IS NULL AND (type_trans='echange' OR type_trans='don'))),
	CHECK (date_debut <= date_fin OR date_fin IS NULL)
);

CREATE TABLE Commentaire
(
	id_com INTEGER,
	contenu_com VARCHAR NOT NULL,
	dates_com DATE NOT NULL,
	uti INTEGER NOT NULL,
	annonce INTEGER, 
	FOREIGN KEY (uti) REFERENCES Utilisateur(id_uti),
	FOREIGN KEY (annonce) REFERENCES Annonce(id_annonce), 
	PRIMARY KEY (id_com, annonce)
);

CREATE TABLE MotsCles
(
	mot VARCHAR PRIMARY KEY
);

CREATE TABLE AnnonceMots
(
	annonce INTEGER,
	mot VARCHAR,
	FOREIGN KEY (annonce) REFERENCES Annonce(id_annonce),
	FOREIGN KEY (mot) REFERENCES MotsCles(mot),
	PRIMARY	KEY (annonce,mot)
);

CREATE TABLE Rubrique
(
	nom_rubrique VARCHAR PRIMARY KEY
);

CREATE TABLE AcorrespondR
(
	annonce INTEGER,
	rubrique VARCHAR,
	FOREIGN KEY (annonce) REFERENCES Annonce(id_annonce),
	FOREIGN KEY (rubrique) REFERENCES Rubrique(nom_rubrique),
	PRIMARY KEY (annonce,rubrique) 
);

CREATE TABLE Proposition
(
	id_proposition INTEGER PRIMARY KEY,
	etat VARCHAR CHECK(etat = 'FINAL' OR etat = 'En cours'),
	montant_prop REAL,
	date_prop DATE NOT NULL,
	annonce INTEGER NOT NULL,
	admin_annule INTEGER,
	id_acheteur INTEGER NOT NULL, 
	FOREIGN KEY (annonce) REFERENCES Annonce(id_annonce),
	FOREIGN KEY (admin_annule) REFERENCES Administrateur(id_admin), 
	FOREIGN KEY (id_acheteur) REFERENCES Utilisateur (id_uti)
);

/*Ajout de vues*/

    /*Vue permettant d'afficher toutes les annonces non supprimés*/
        
        CREATE VIEW vAnnonceVisible AS
            SELECT *
            FROM Annonce
            WHERE admin_delete IS NULL;


    /*Vue permettant de voir les transactions réalisés sur le site (décomposition en 6 vues distinctes)*/
    
        CREATE VIEW vTransaction1 AS
            SELECT * FROM Proposition
            WHERE Etat='FINAL';
        
        CREATE VIEW vTransaction2 AS 
            SELECT * FROM Annonce a, vTransaction1 T1 
            WHERE a.id_annonce=T1.annonce; 
        
        CREATE VIEW vTransaction3 AS 
            SELECT *  FROM Utilisateur u, vTransaction2 T2 
            WHERE u.id_uti=T2.uti; 
        
        CREATE VIEW vTransaction4 (id_proposition, montant_prop, date_prop, id_acheteur, nom_vendeur, prenom_vendeur, id_vendeur, id_annonce, intitule) AS 
            SELECT T3.id_proposition, T3.montant_prop, T3.date_prop, T3.id_acheteur, T3.nom, T3.prenom, T3.id_uti, T3.id_annonce, T3.intitule
            FROM vTransaction3 T3; 
        
        CREATE VIEW vTransaction5 AS
            SELECT * FROM vTransaction4 T4, Utilisateur u 
            WHERE T4.id_acheteur=u.id_uti; 
        
        CREATE VIEW vTransaction (id_transaction, id_annonce, intitule_annonce, montant, date_transaction, id_acheteur, nom_acheteur, prenom_acheteur, id_vendeur, nom_vendeur, prenom_vendeur) AS 
            SELECT T5.id_proposition, T5.id_annonce, T5.intitule, T5.montant_prop, T5.date_prop, T5.id_acheteur, T5.nom, T5.prenom, T5.id_vendeur, T5.nom_vendeur, T5.prenom_vendeur
            FROM vTransaction5 T5; 
            

    
    /*Vue permettant de voir tout les utilisateurs actuellement bloqué (décomposition en 3 vues distinctes)*/
    
        CREATE VIEW vUtilisateurBloque1 (uti_bloque,date_bloquage) AS
        	SELECT A.uti, MAX(A.date_act)
        	FROM Action A
        	GROUP BY A.uti
        	ORDER BY A.uti;
        
        CREATE VIEW vUtilisateurBloque2 (uti_bloque,date_bloquage,raison,etat,admin) AS
        	SELECT A.uti,B.date_bloquage,A.raison,A.type,A.admin
        	FROM Action A LEFT JOIN vUtilisateurBloque1 B ON A.uti = B.uti_bloque
        	WHERE A.type = 'bloque' AND A.date_act=B.date_bloquage;
        
        CREATE VIEW vUtilisateurBloque (Nom,Prenom,Date_Bloquage,Raison_Bloquage,Etat,Admin) AS
        	SELECT U.nom, U.prenom,B2.date_bloquage,B2.raison,B2.etat,B2.admin
        	FROM Utilisateur U JOIN vUtilisateurBloque2 B2 ON B2.uti_bloque = U.id_uti;

    /*Autres vues permettant d'afficher les annonces correspondant respectivement à : des négociations, des échanges, des dons, des affaires normales et des encheres*/
    
    CREATE VIEW vANegocier AS
    	SELECT A.id_annonce, A.type_annonce, A.intitule,A.description,A.date_debut,A.date_fin,A.stock,A.type_produit,A.ville_nom,A.ville_cp,A.prix_haut
    	FROM Annonce A
    	WHERE A.type_trans = 'a_negocier' AND admin_delete IS NULL;
    
    CREATE VIEW vEchange AS
    	SELECT A.id_annonce, A.type_annonce, A.intitule,A.description,A.date_debut,A.date_fin,A.stock,A.type_produit,A.ville_nom,A.ville_cp
    	FROM Annonce A
    	WHERE A.type_trans = 'echange' AND admin_delete IS NULL;
    
    CREATE VIEW vDon AS
    	SELECT A.id_annonce, A.type_annonce, A.intitule,A.description,A.date_debut,A.date_fin,A.stock,A.type_produit,A.ville_nom,A.ville_cp
    	FROM Annonce A
    	WHERE A.type_trans = 'don' AND admin_delete IS NULL;
    
    CREATE VIEW vAffaireNormale AS
    	SELECT A.id_annonce, A.type_annonce, A.intitule,A.description,A.date_debut,A.date_fin,A.stock,A.type_produit,A.ville_nom,A.ville_cp,A.prix
    	FROM Annonce A
    	WHERE A.type_trans = 'affaire_normale' AND admin_delete IS NULL;
    
    CREATE VIEW vEnchere AS
    	SELECT A.id_annonce, A.type_annonce, A.intitule,A.description,A.date_debut,A.date_fin,A.stock,A.type_produit,A.ville_nom,A.ville_cp,A.prix_bas
    	FROM Annonce A
    	WHERE A.type_trans = 'enchere' AND admin_delete IS NULL;




