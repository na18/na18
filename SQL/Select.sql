/* Selection des messages entre 2 utilisateurs*/
SELECT M.date_mes, M.contenu_message, U.nom, U.prenom
FROM Message M JOIN Utilisateur U ON uti_en = id_uti
WHERE uti_en = 6 AND uti_re = 7 OR uti_en = 7 AND uti_re = 6;

/*Selection par rubrique */
SELECT * FROM AnnonceVisible av, AcorrespondR acr
WHERE av.id_annonce=acr.annonce
AND acr.rubrique='Sport';

/*Selection par mots clés*/
SELECT * FROM AnnonceVisible av, AnnonceMots am
WHERE av.id_annonce=am.annonce
AND am.mot='Toile';
