# Preuves de normalisation

 Dans l'ensemble des preuves, les clés sont déduite grâce à la couverture minimale et la dépendance fonctionnelle de l'ensemble des attributs est déduite grâce à la cloture transitive.

## Utilisateur

CM = { ID_Uti -> Email, Nom, Prenom, Num_tel, Mot_de_passe, Date_naissance, Solde, adresse;

​	 Email -> ID_Uti }

F+ = { ID_Uti -> Email, Nom, Prenom, Num_tel, Mot_de_passe, Date_naissance, Solde, adresse;

​	 Email -> ID_Uti, Nom, Prenom, Num_tel, Mot_de_passe, Date_naissance, Solde, adresse; }

On a ID et Email comme clé. Adresse mail n'est pas immuable, donc on a ID clé primaire.

#### Preuve

Cette table est en 3NF. On a toutes les DF de la forme K→A avec K une clé.

On pourrait penser que le numéro de téléphone détermine d'autres attributs. En effet, le téléphone peut être fixe et donc plusieurs personnes auront le même numéro. Un couple (ou des colocataires) aura une adresse commune.



## Administrateur

CM = F+ = {ID_Admin}

#### Preuve

Cette table est en BCNF car 1 seul attribut clé. 



## Action

CM = F+ = {ID_Ac -> Type,admin,uti}

#### Preuve

Cette table est en BCNF. On a toutes les DF de la forme K→A avec K une clé.



## Adresse

CM = F+ = { ID_Adresse -> Num_Rue, Nom_Rue, ville_nom, ville_cp  }

On prend en compte ici le fait que plusieurs villes puissent avoir le même code postale

#### Preuve

Cette table est en BCNF. On a toutes les DF de la forme K→A avec K une clé.



## Ville

CM = F+ = { (Nom, CP) -> Pays }

On a (nom, cp) comme clé.

#### Preuve

Cette table est en BCNF. On a toutes les DF de la forme K→A avec K une clé.



## Messages

CM = F+ =  { ID_Message -> Contenu_Messsage,  Date, Uti1, Uti2 ; }

On a uniquement ID_Message comme clé.

#### Preuve

Ainsi, cette table est BNCF. On a toutes les DF de la forme K→A avec K une clé.




## Annonce

CM = F+ =  { ID_Annonce -> Type_Annonce,  Date_debut, Date_Fin,Intitule, Description, Stock, TypeProduit, type_trans, ville_nom, ville_cp, admin_delete, uti, Prix, Prix_bas, Prix_haut ; }

On a uniquement ID_Annonce comme clé.

#### Preuve

Ainsi, cette table est BNCF. On a toutes les DF de la forme K→A avec K une clé.



##### Remarque

On avait au départ placé un attribut "mots_cles" dans la classe annonce, celui ci étant multivalué la classe n'était pas en 1NF à cause du problème d'atomicité de 'mots_cles'.




## Mots_Cles

CM = F+ = {mot}

#### Preuve

Cette table est en BCNF car un seul attribut clé.




## Annonce_Mot

CM = F+ = {(Mot_cle,Annonce)}

#### Preuve

Cette classe est en BCNF car seulement 2 attributs formant une clé et ne se définissant pas entre eux.



## Rubrique

CM = F+ = {Nom_Rubrique}

#### Preuve

Cette classe est en BCNF car un seul attribut formant la clé.




## AcorrespondR

CM = F+ = {(annonce,rubrique)}

#### Preuve

Cette classe est en BCNF car seulement 2 attributs formant une clé et ne se définissant pas entre eux.




## Commentaire

CM = F+ =  { (ID_Comment, Annonce) -> Contenu_Commentaire, Date ; }

#### Preuve

Ainsi, cette table est 3NF. On a toutes les DF de la forme K→A avec K une clé.


## Proposition

CM = F+ ={ID_Proposition -> etat, montant_prop, date_prop, annonce, admin_annule, id_acheteur}

#### Preuve

Cette table est en BCNF car on a toutes les DF de la forme K→A avec K une clé.




##### Remarque

On pourrait remplacer ID_Ac de classe action, ID_Message de classe Messages et ID_comment de classe Commentaire par une date avec une heure en supposant qu'il est impossible que 2 informations soient enregistrée strictement en même temps. Cependant il faut que la date soit assez précise pour que la gestion de l'ajout de donnée de façon simultanée puisse être géré avec des transactions. 