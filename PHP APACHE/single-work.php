<?php
    session_start(); 
    include "utils.php";
    function PrintAnnonce(){
      $vSql="SELECT description,date_debut,type_produit,email 
      FROM Annonce,Utilisateur
      Where Annonce.uti = Utilisateur.id_uti
      ORDER BY date_debut DESC";
      $annonces=fSelect($vSql);

      echo "
      <table>
        <tr><th>User</th><th>Description</th><th>Date Debut</th><th>Type Produit</th></tr>";

      $i=0;
      while($i<count($annonces)){ 
        echo "<tr><td>".$annonces[$i]["email"]."</td><td>"
                      .$annonces[$i]["description"]."</td><td>"
                      .$annonces[$i]["date_debut"]."</td><td>"
                      .$annonces[$i]["type_produit"]."</td><td>";
        $i++;
      }
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Home Page</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">


  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700&subset=latin,cyrillic-ext" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/photostack/photostack.css" rel="stylesheet">
  <link href="lib/fullpage-menu/fullpage-menu.css" rel="stylesheet">
  <link href="lib/cubeportfolio/cubeportfolio.css" rel="stylesheet">
  <link href="lib/superslides/superslides.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
</head>

<body>

  <!-- Custom navbar -->

  <div>
    <ul class="fm-first-level">
    	<li><a href="single-work.php" class="btn btn-lg btn-transparent">HOME</a></li>
		<li><a href="signin.html" class="btn btn-lg btn-transparent">SIGN IN</a></li>
      	<li><a href="codephp.php" class="btn btn-lg btn-transparent">Affichage</a></li>
    </ul>
  </div>

  <!-- /END CUSTOM NAVBAR -->

 
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-folder-outline large-icon"></i>
        <h1>CHANGE DANS UTC</h1>
        <hr>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row mt presentation">
      <!--<div class="col-lg-4 col-md-4">-->
        <h3>Recherche dans annonces</h3> 
        <form method="post" action="select.php">
          <div class="form-col">
            <div class="form-group">
                <label for="signin_form">Type Annonce</label>
                <input name="Type" type="text" class="form-control" id="signin_form"
                placeholder="affaire_normale/enchere/a_negocier/don">
            </div><!--/.form-group -->
          </div><!--/.form-col -->
          <div class="form-col1">
            <div class="form-group">
              <label for="signin_form">Date</label>
                <input name="Date" type="date" class="form-control" id="signin_form" >
            </div><!--/.form-group -->
          </div>
          <div class="signin-footer">
            <input type="submit" class="btn signin_btn" data-toggle="modal" data-target=".signin_modal" value="Submit">
          </div>

    </div>

  </div>

  <div class="container">
    <div class="row mt presentation">
      <!--<div class="col-lg-4 col-md-4">-->
        <h3>Annonces</h3> 
        <span><?php PrintAnnonce(); ?></span>
    </div>

  </div>

    <!-- /row -->
<!--
      <div class="col-lg-4 col-md-4">
        <p><b>ID:</b> June 2014</p>
        <p>
          <b>Category:</b>
          <pl>Print, Design</pl><br/>
          <b>Client:</b>
          <pl>Offscreen Magazine</pl>
        </p>
        <p><b>Lead Designer:</b>
          <pl>Marcel Newman</pl>
        </p>
         
      </div>
-->

<!--
  <section id="contact" class="section-bg-color mt2">
    <div class="container centered ptb">
      <div class="col-md-2 col-md-offset-1">
        <i class="icon ion-ios7-location-outline large-icon"></i>
        <h5>HEAD OFFICE</h5>
        <p>902 Main Street, MVD, UY.</p>
      </div>
      <div class="col-md-2 col-md-offset-2">
        <i class="icon ion-ios7-telephone-outline large-icon"></i>
        <h5>CALL US</h5>
        <p>(598) 123-4567</p>
      </div>
      <div class="col-md-2 col-md-offset-2">
        <i class="icon ion-ios7-paperplane-outline large-icon"></i>
        <h5>QUERIES</h5>
        <p>support@mail.com</p>
      </div>
    </div>
  </section>
-->


  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/modernizr/modernizr.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/php-mail-form/validate.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/cubeportfolio/cubeportfolio.js"></script>
  <script src="lib/classie/classie.js"></script>
  <script src="lib/fullpage-menu/fullpage-menu.js"></script>
  <script src="lib/photostack/photostack.js"></script>
  <script src="lib/superslides/superslides.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
