# Note de clarification
Réaliser d'une base de données pour d'une place d'échange à l'UTC.

Tous les documents seront disponible sur ce [repo GitLab](https://gitlab.utc.fr/na18/na18)
[]()
## LISTE DES OBJETS

### Utilisateur
- Identifiant (Entier) 
- Email (Chaîne)
- Nom (Chaîne）
- Prénom (Chaîne)
- Numéro de téléphone (Entier)
- Mot de passe (Chaîne)
- Date de naissance (Date)
- Solde (Réel)

L'identifiant sera unique et non nul et l'adresse email devra être unique.

### Administrateur 
- Id (Entier)

L'identifiant devra être unique et non nul.

### Adresse
- Id_adresse (Entier) 
- Num_Rue (Entier)
- Nom_Rue (Chaîne)

L'identifiant devra être unique et non nul.

### Ville 
- Nom (Chaîne) 
- Code Postal (Entier) 
- Pays (Chaîne)

Le Nom de la ville couplée à sont code postale sera unique et non nul.

### Annonce
- Id_Annonce (Entier)
- Intitulé (Chaîne)
- TypeAnnonce {'Offre', 'Demande'} (Variable)
- Date debut (Date)
- Date fin (Date)
- Description (Chaîne)
- Stock (Entier)
- Type {'Bien', 'Service'} (Variable)

L'identifiant devra être unique et non nul.

### Enchère - Hérite TRANSACTION
- Prix bas (Réel)


### A Negocier - Hérite TRANSACTION
- Prix haut (Réel)


### Echange - Hérite TRANSACTION

### Don - Hérite TRANSACTION

### Affaire_Normal - Hérite TRANSACTION
- Prix (Réel)

### Rubrique
- nom_rubrique (Chaîne)

Le nom de la rubrique sera unique et non nul.

### Mots Clés
- Mot (Chaîne)

Le Mot devra être unique et non nul.

### Proposition 
- ID_Proposition (Entier)
- Etat (Chaîne) {'Final','En cours'}
- Montant_prop (Reel) 
- Date_prop (Date)

L'identifiant devra être unique et non nul.

### Action 
- Identifiant (Entier)
- Type (Chaîne) {'Bloque', 'Debloque'}
- date (Date)
- raison (Chaîne)


L'identifiant devra être unique et non nul.


### Message
- ID (Entier)
- Date (Date)
- Contenu message (Chaîne Longue)

L'identifiant devra être unique et non nul.

### Commentaire 
- Id (Entier) 
- Date (Date)
- Contenu message (Chaîne Longue)

L'identifiant devra être unique et non nul.

## Heritage 
Heritage par classe mère pour les annonces. 

## Vues 
Réaliser une vue pour afficher les conversations(ensemble de messages).

Réaliser une vue pour afficher les transactions (dernière proposition).

Réaliser une vue pour afficher les utilisateurs bloqués.

Réaliser une vue pour afficher toutes les annonces dans une ville. 

Réaliser une vue pour afficher l'historique d'un utilisateur. 

 





## Fonctionalité
Pour visualiser l’historique, nous utiliserons l’identifiant de l’utilisateur pour retrouver dans la table des transactions(cf Vues) et des annonces toutes celles dont il fait partie. 

Les utilisateurs peuvent consulter une offre/demande mais ne peuvent pas la modifier s’ils n’en sont pas l'émetteur. 

L’utilisateur pourra rechercher dans la description à l’aide de mots clés les offres ou demandes qui l’intéresse. 

L’utilisateur peut faire une offre/demande et répondre à une offre/demande.

Un utilisateur ne possède qu'une seule adresse.

Pour les annonces, il existe quatre cas de figures : 

1. AFFAIRE NORMALE : le prix est fixe. 
2. A NEGOCIER : négociation, un prix maximal est fixé par l’émetteur de l’offre et négocier par le receveur. 
3. ENCHERE:  un prix minimal est fixé par l’émetteur et augmente en fonction des offres des différents acheteurs intéressés. De plus, le vendeur doit fixé une date de fin d’enchère. 
4. ECHANGE : un don est considéré comme une offre dont le prix est nul. 

Une offre peut rentrer dans l’un de ces quatres cas de figures. 
Une demande peut fonctionner sur le don, le prix fixe ou la négociation. 

Une offre/demande peut-être définie par plusieurs mots-clés. 

Bien et service sont une sous-catégorie de demande et offre. 

## Remarques

Utilisateurs simples utilisant le site : Accès à un compte, ajout d’offres/demandes, gestion d’un porte monnaie, accès à une messagerie, accès à la recherche dans les données (offre/demande/contact)

Administrateurs : Accès à l’intégralité des fonctions, à la lecture et modification des données, modération comptes utilisateurs, gestion des transactions, Suppression des annonces