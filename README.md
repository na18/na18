# NA18 Conception de base de données relationnelles (A19 Groupe9)


## Sujet
Le but de ce projet est de réaliser une place d'échange à l'UTC. 

## Projet
Projet final est sur lien de [http://tuxa.sme.utc/~bdd0a003/Apache/single-work.php](http://tuxa.sme.utc/~bdd0a003/Apache/single-work.php)

## Calendrier
- 16/09/2019 - 22/09/2019 Note de clarification avec tous les objets et attributs.
- 23/09/2019 - 30/09/2019 Modélisation de conceptuelle de données (UML), changement de NDC V2.
- 31/09/2019 - 07/10/2019 Modélisation de logique de données, changement de MDC.
- 08/10/2019 - 14/10/2019 SQL LDD
- 15/10/2019 - 22/10/2019 SQL LMD (Select)
- 11/11/2019 - 20/11/2019 SQL (Vue, Group by)


## Membres du groupe
BOURÉ Bastien, JURADO Solange, SELINCE Tiffany, XIE Yijue et YAN Dan